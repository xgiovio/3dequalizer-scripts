
```
#!notes

Collection of 3dequalizer scripts written in Python.
Software used 3dequalizer 4 r4.
Scripts are in pure python.
Interpreter 2.6.4

Install
- Copy all scripts, except (single_versions_for_3de_online_db_scripts folder) in 3de root /sys_data/py_scripts
- Done
- Launch 3de

For fixes, requests or other bugs, please create an issue request or send an email to xgiovio@gmail.com

Videotutorials: https://www.youtube.com/playlist?list=PLsdWQqdM2JqWeCLgAbT_Ka3mPtyOtP8zy

Notes : The single_versions_for_3de_online_db_scripts folder contains the same scripts but without dependencies.
		These scripts have been merged and so there is redundant code. This limitation is caused by 3de online submission scripts form.
		You can't upload multiple files with dependencies. So to make things work I had to join codes.


Groups of scripts:

xg_paste_2d_curve.py - dependency: xg_copy_paste_2d_curves.py 
xg_paste_def_to_keys_2d_curve.py - dependency: xg_copy_paste_2d_curves.py 
xg_paste_def_to_keys_inside_2d_curve.py - dependency: xg_copy_paste_2d_curves.py 
xg_copy_2d_curve.py - dependency: xg_copy_paste_2d_curves.py 
xg_copy_paste_2d_curves.py

xg_delete_2d_curve.py

xg_backup_2d_curves.py - dependency: xg_backup_restore_2d_curves.py
xg_backup_restore_2d_curves.py
xg_restore_2d_curves.py - dependency: xg_backup_restore_2d_curves.py

xg_fill2d.py
xg_fill2d_average_each_2_points.py - dependency: xg_fill2d.py
xg_fill2d_average_each_2_points_dist.py - dependency: xg_fill2d.py

xg_fill3d.py
xg_fill3d_average_each_2_points.py - dependency: xg_fill3d.py
xg_fill3d_average_each_2_points_reelin.py - dependency: xg_fill3d.py
xg_fill3d_average_each_2_points_reelin_dist.py - dependency: xg_fill3d.py
xg_fill3d_average_each_2_points_reelin_dist_visible.py - dependency: xg_fill3d.py
xg_fill3d_average_each_2_points_reelin_visible.py - dependency: xg_fill3d.py

xg_reconstruct_del_non_keyframes.py - dependency: xg_reconstruct_tool.py
xg_reconstruct_del_non_keyframes_and_triangulate.py - dependency: xg_reconstruct_tool.py
xg_reconstruct_del_non_keyframes_and_triangulate_and_backproj.py - dependency: xg_reconstruct_tool.py
xg_reconstruct_del_non_keyframes_and_triangulate_and_backproj_deform.py - dependency: xg_reconstruct_tool.py
xg_reconstruct_del_non_keyframes_and_triangulate_and_backproj_deform_dist.py - dependency: xg_reconstruct_tool.py
xg_reconstruct_del_non_keyframes_and_triangulate_and_backproj_deform_inside.py - dependency: xg_reconstruct_tool.py
xg_reconstruct_del_non_keyframes_and_triangulate_and_backproj_deform_inside_dist.py - dependency: xg_reconstruct_tool.py
xg_reconstruct_del_non_keyframes_and_triangulate_and_backproj_dist.py - dependency: xg_reconstruct_tool.py
xg_reconstruct_del_non_keyframes_and_triangulate_and_backproj_inside.py - dependency: xg_reconstruct_tool.py
xg_reconstruct_del_non_keyframes_and_triangulate_and_backproj_inside_dist.py - dependency: xg_reconstruct_tool.py
xg_reconstruct_tool.py

xg_2d_curve_editor.py - dependency: xg_2d_curve_editor_adj_script.py
xg_2d_curve_editor_adj_script.py - dependency: xg_2d_curve_editor.py

xg_restore_3d_calculated_points.py - dependency: xg_store_restore_3d_points.py
xg_restore_3d_calculated_points_exact.py - dependency: xg_store_restore_3d_points.py
xg_store_3d_calculated_points.py - dependency: xg_store_restore_3d_points.py
xg_store_restore_3d_points.py

xg_export_photoscan.py

```