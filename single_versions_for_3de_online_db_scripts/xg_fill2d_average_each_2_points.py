#
#
# 3DE4.script.name:	Average each 2 points 2D
#
# 3DE4.script.version:		v1.0
#
# 3DE4.script.comment:	Average each 2 2D points
#
# 3DE4.script.gui:	Manual Tracking Controls::Fill2D
# 3DE4.script.hide: false
# 3DE4.script.startup: false
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#

import itertools

def average_each_2_points (dist = False):
	cam = tde4.getCurrentCamera()
	playback = tde4.getCameraPlaybackRange(cam)
	firstframe = playback[0]
	lastframe = playback[1]
	current_point_group = tde4.getCurrentPGroup()
	selected_points = tde4.getPointList (current_point_group,1)
	if (len(selected_points) < 2):
		tde4.postQuestionRequester("Info","You need to select 2 or more 2D points","OK")
	else:
		exit = 0
		tde4.postProgressRequesterAndContinue("Calculating","Computing Points",100,"Cancel")
		couples_selected_points = list(itertools.combinations(selected_points,2))
		counter = 0
		for i in couples_selected_points:
			if exit == 1:
				break
			avg = tde4.createPoint (current_point_group)
			aname = tde4.getPointName(current_point_group,i[0])
			bname = tde4.getPointName(current_point_group,i[1])
			tde4.setPointName(current_point_group,avg, "AVG_" + aname + "_" + bname)
			for frame in range (firstframe, lastframe + 1):
				counter+=1
				percentage = (100 * counter) / (len(couples_selected_points) * (lastframe + 1 - firstframe))
				status = tde4.updateProgressRequester(percentage,str(percentage) + " % - Frame " + str(frame) + " Point AVG_" + aname + "_" + bname)
				if status != -1:
					exit = 1
					break
				apos = tde4.getPointPosition2D (current_point_group,i[0],cam,frame)
				if dist:
					apos = tde4.removeDistortion2D(cam,frame,apos)
				avalid =tde4.isPointPos2DValid(current_point_group,i[0],cam,frame)
				aposx = apos[0]
				aposy = apos[1]
				bpos = tde4.getPointPosition2D (current_point_group,i[1],cam,frame)
				if dist:
					bpos = tde4.removeDistortion2D(cam,frame,bpos)
				bvalid =tde4.isPointPos2DValid(current_point_group,i[1],cam,frame)
			 	bposx = bpos[0]
			 	bposy = bpos[1]
			 	if avalid and bvalid:
				 	avgpos = [ aposx + ( (bposx - aposx) / 2 ) , aposy + ( (bposy - aposy) / 2 ) ]
				 	if dist:
				 		avgpos = tde4.applyDistortion2D(cam,frame,avgpos)
				 	tde4.setPointPosition2D (current_point_group,avg,cam,frame,avgpos)


average_each_2_points (dist = False)