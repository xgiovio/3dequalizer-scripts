#
#
# 3DE4.script.name:	Delete Non Keyframes and Triangulate
#
# 3DE4.script.version:		v1.0
#
# 3DE4.script.comment:	Delete Non Keyframes,Triangulate
#
# 3DE4.script.gui:	Manual Tracking Controls::Reconstruction
# 3DE4.script.gui.button:	Manual Tracking Controls::Del Non Keyframes + Trian, align-bottom-left, 120, 20


# 3DE4.script.hide: false
# 3DE4.script.startup: false
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#



def operations (del_non_keyframes = False, triangulate = False, reelin = False, distortion = False , deform = False, inside_only = False):


	cam = tde4.getCurrentCamera()
	current_point_group = tde4.getCurrentPGroup()
	selected_points = tde4.getPointList (current_point_group,1)
	playback = tde4.getCameraPlaybackRange(cam)
	firstframe = playback[0]
	lastframe = playback[1]

	if del_non_keyframes :
		for point in selected_points :
			k_pos = []
			for frame in range (firstframe, lastframe + 1):
				point_status = tde4.getPointStatus2D(current_point_group,point,cam,frame)
				if point_status == "POINT_KEYFRAME" or point_status == "POINT_KEYFRAME_END":
					pos = tde4.getPointPosition2D(current_point_group,point,cam,frame)
					k_pos.append([frame,[  pos[0],  pos[1] ] ])
			tde4.deletePointCurve2D (current_point_group,point,cam)
			for i in k_pos :
				tde4.setPointPosition2D(current_point_group,point,cam,i[0],i[1])
			if triangulate :
				if len(k_pos) >= 2:
					pos3d = tde4.calcPointPosition3D(current_point_group,point,cam)
					tde4.setPointCalcPosition3D(current_point_group,point,pos3d)
					tde4.setPointCalculated3D(current_point_group,point,1)
					tde4.setPointCalculated3DStatus(current_point_group,point,"TRIANGULATED")
					if reelin :
						points_to_deform = {}
						for frame in range (firstframe, lastframe + 1):
							if distortion:
								pos = tde4.calcPointBackProjection2D(current_point_group,point,cam,frame,1)
							else:
								pos = tde4.calcPointBackProjection2D(current_point_group,point,cam,frame,0)
							if not deform:
								if inside_only :
									if frame >= k_pos[0][0] and frame <= k_pos[-1][0]:
										tde4.setPointPosition2D(current_point_group,point,cam,frame,pos)
								else:
									tde4.setPointPosition2D(current_point_group,point,cam,frame,pos)
							else:
								points_to_deform[frame] = pos
					if deform:

						#update k_pos with offset from reelincurve
						for i in k_pos:
							reel_in_x = points_to_deform[i[0]][0]
							reel_in_y = points_to_deform[i[0]][1]
							k_pos_x = i[1][0]
							k_pos_y = i[1][1]
							offset_x = k_pos_x - reel_in_x
							offset_y = k_pos_y - reel_in_y
							i[1] = [offset_x,offset_y]

						#extend keyframes to reelin range
						k_pos_extended = []
						if k_pos[0][0] != firstframe and not inside_only:
							k_pos_extended.append([firstframe,k_pos[0][1]])
						for i in k_pos:
							k_pos_extended.append(i)
						if k_pos[-1][0] != lastframe and not inside_only:
							k_pos_extended.append([lastframe,k_pos[-1][1]])


						for i in range(len(k_pos_extended) - 1):
							f0 = k_pos_extended[i][0]
							f1 = k_pos_extended[i+1][0]
							xo0 = k_pos_extended[i][1][0]
							yo0 = k_pos_extended[i][1][1]
							xo1 = k_pos_extended[i+1][1][0]
							yo1 = k_pos_extended[i+1][1][1]
							# linear deform step
							for f in range(f0, f1):
								xoI = (xo0 + ((f - f0) * ((xo1 - xo0) / (f1 - f0))))
								yoI = (yo0 + ((f - f0) * ((yo1 - yo0) / (f1 - f0))))
								reelin_curve_x = points_to_deform[f][0]
								reelin_curve_y = points_to_deform[f][1]
								tde4.setPointPosition2D(current_point_group,point,cam,f,[reelin_curve_x + xoI,  reelin_curve_y + yoI ] )
						#lastkeyframe
						reelin_curve_x = points_to_deform[f1][0]
						reelin_curve_y = points_to_deform[f1][1]
						tde4.setPointPosition2D(current_point_group,point,cam,f1,[reelin_curve_x + xo1,  reelin_curve_y + yo1 ] )

operations (del_non_keyframes = True, triangulate = True)
