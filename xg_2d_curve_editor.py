#
#
# 3DE4.script.name:	Adjust 2D Curve Local Filter 
#
# 3DE4.script.version:		v1.1
#
# 3DE4.script.comment:	Adjustment of 2d Points positions using 1D Fourier filter on different frames ranges
#
# 3DE4.script.gui:	Main Window::Adjustment
#
# 3DE4.script.hide: false
# 3DE4.script.startup: false
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#


script_base = tde4.get3DEInstallPath() + "/sys_data/py_scripts/"

#script related
adj_script_name = "xg_2d_curve_editor_adj_script.py"


editor_width = 1200
editor_height = 800

curvex = 0
curvey = 0
curvex_filtered_min = 0
curvex_filtered_max = 0
curvey_filtered_min = 0
curvey_filtered_max = 0
curvex_reloaded = 0
curvey_reloaded = 0
loaded_point = ""
loaded_cam = ""
import_status = False

adj_parameters_list = []

first_time_range_adj = True


def callback_editor (req):
	cam = tde4.getCurrentCamera()
	tde4.setCurveAreaWidgetCursorPosition(req,"CurveAreaWidgetX",tde4.getCurrentFrame(cam),1)
	tde4.setCurveAreaWidgetCursorPosition(req,"CurveAreaWidgetY",tde4.getCurrentFrame(cam),1)

def list_adjs (req):
	tde4.removeAllListWidgetItems(req,"ListAdj")
	for i in range(len(adj_parameters_list)):
		tde4.insertListWidgetItem(req,"ListAdj",adj_parameters_list[i],i)


def callback (req,label,event):

	global adj_parameters_list
	global loaded_point
	global loaded_cam
	global import_status
	global first_time_range_adj

	if label == "LoadCurve":

		cam = tde4.getCurrentCamera()
		current_point_group = tde4.getCurrentPGroup()
		selected_point = tde4.getPointList (current_point_group,1)
		playback = tde4.getCameraPlaybackRange(cam)
		firstframe = playback[0]
		lastframe = playback[1]

		if first_time_range_adj:
			tde4.setWidgetValue(req,"Range_Min",str(firstframe))
			tde4.setWidgetValue(req,"Range_Max",str(lastframe))
			first_time_range_adj = False
		

		if len(selected_point) != 1 :
			tde4.postQuestionRequester("Error","You need to select 1 2D point only","OK")
			tde4.setWidgetSensitiveFlag(req,"FilterAllX",0)
			tde4.setWidgetSensitiveFlag(req,"FilterAllY",0)
			tde4.setWidgetSensitiveFlag(req,"FilterAllXAdd",0)
			tde4.setWidgetSensitiveFlag(req,"FilterAllYAdd",0)
			tde4.setWidgetSensitiveFlag(req,"FilterLocX",0)
			tde4.setWidgetSensitiveFlag(req,"FilterLocY",0)
			tde4.setWidgetSensitiveFlag(req,"FilterLocXAdd",0)
			tde4.setWidgetSensitiveFlag(req,"FilterLocYAdd",0)
		elif lastframe - firstframe < 2 :
			tde4.postQuestionRequester("Error","You need a camera with at least 3 frames","OK")
			tde4.setWidgetSensitiveFlag(req,"FilterAllX",0)
			tde4.setWidgetSensitiveFlag(req,"FilterAllY",0)
			tde4.setWidgetSensitiveFlag(req,"FilterAllXAdd",0)
			tde4.setWidgetSensitiveFlag(req,"FilterAllYAdd",0)
			tde4.setWidgetSensitiveFlag(req,"FilterLocX",0)
			tde4.setWidgetSensitiveFlag(req,"FilterLocY",0)
			tde4.setWidgetSensitiveFlag(req,"FilterLocXAdd",0)
			tde4.setWidgetSensitiveFlag(req,"FilterLocYAdd",0)
		else :
			tde4.setWidgetSensitiveFlag(req,"FilterAllX",1)
			tde4.setWidgetSensitiveFlag(req,"FilterAllY",1)
			tde4.setWidgetSensitiveFlag(req,"FilterAllXAdd",1)
			tde4.setWidgetSensitiveFlag(req,"FilterAllYAdd",1)
			tde4.setWidgetSensitiveFlag(req,"FilterLocX",1)
			tde4.setWidgetSensitiveFlag(req,"FilterLocY",1)
			tde4.setWidgetSensitiveFlag(req,"FilterLocXAdd",1)
			tde4.setWidgetSensitiveFlag(req,"FilterLocYAdd",1)

			name = tde4.getPointName(current_point_group,selected_point[0])
			tde4.setWidgetValue(req,"PointName",name)
			tde4.setWidgetValue(req,"PointID",selected_point[0])

			selected_point_positions = []
			for frame in range (firstframe, lastframe + 1):
				if tde4.isPointPos2DValid(current_point_group,selected_point[0],cam,frame):
					selected_point_position = tde4.getPointPosition2D(current_point_group,selected_point[0],cam,frame)
					selected_point_positions.append([frame,selected_point_position[0],selected_point_position[1]])


			global curvex
			global curvey
			global curvex_filtered_min
			global curvex_filtered_max
			global curvey_filtered_min
			global curvey_filtered_max
			global curvex_reloaded
			global curvey_reloaded
			

			if curvex == 0 or loaded_point != selected_point[0] or loaded_cam!= cam:

				tde4.detachCurveAreaWidgetAllCurves(req,"CurveAreaWidgetX")
				tde4.detachCurveAreaWidgetAllCurves(req,"CurveAreaWidgetY")


				curvex = tde4.createCurve()
				curvey = tde4.createCurve()
				curvex_filtered_min = tde4.createCurve()
				curvex_filtered_max = tde4.createCurve()
				curvey_filtered_min = tde4.createCurve()
				curvey_filtered_max = tde4.createCurve()
				curvex_reloaded = 0
				curvey_reloaded = 0
				loaded_point = selected_point[0]
				loaded_cam= cam

				tde4.removeAllListWidgetItems(req,"ListAdj")
				adj_parameters_list = []

				frame_min = 0
				frame_max = 0
				x_y_min = 0
				x_y_max = 0
				y_y_min = 0
				y_y_max = 0

				first_x = True
				first_y = True
				for i in selected_point_positions:
					c = tde4.createCurveKey(curvex,[i[0],i[1]])
					#tde4.setCurveKeyMode(curvex,c,"LINEAR")
					#tde4.setCurveKeyFixedXFlag(curvex,c,1)
					if first_x:
						frame_min = i[0]
						frame_max = i[0]
						x_y_min = i[1]
						x_y_max = i[1]
						first_x = False
					else:
						if i[0] < frame_min:
							frame_min = i[0]
						if i[0] > frame_max:
							frame_max = i[0]
						if i[1] < x_y_min:
							x_y_min = i[1]
						if i[1] > x_y_max:
							x_y_max = i[1]


					c = tde4.createCurveKey(curvey,[i[0],i[2]])
					#tde4.setCurveKeyMode(curvey,c,"LINEAR")
					#tde4.setCurveKeyFixedXFlag(curvey,c,1)
					if first_y:
						y_y_min = i[2]
						y_y_max = i[2]
						first_y = False
					else:
						if i[2] < y_y_min:
							y_y_min = i[2]
						if i[2] > y_y_max:
							y_y_max = i[2]


				tde4.attachCurveAreaWidgetCurve(req,"CurveAreaWidgetX",curvex,1,1,1,0)
				tde4.attachCurveAreaWidgetCurve(req,"CurveAreaWidgetY",curvey,1,1,1,0)

				if not import_status:
					if frame_min == frame_max:
						tde4.setCurveAreaWidgetDimensions (req,"CurveAreaWidgetX",firstframe,lastframe,0,1)
						tde4.setCurveAreaWidgetFOV (req,"CurveAreaWidgetX",firstframe,lastframe,0,1)
						tde4.setCurveAreaWidgetDimensions (req,"CurveAreaWidgetY",firstframe,lastframe,0,1)
						tde4.setCurveAreaWidgetFOV (req,"CurveAreaWidgetY",firstframe,lastframe,0,1)
					else:
						tde4.setCurveAreaWidgetDimensions (req,"CurveAreaWidgetX",frame_min,frame_max,x_y_min,x_y_max)
						tde4.setCurveAreaWidgetFOV (req,"CurveAreaWidgetX",frame_min,frame_max,x_y_min,x_y_max)
						tde4.setCurveAreaWidgetDimensions (req,"CurveAreaWidgetY",frame_min,frame_max,y_y_min,y_y_max)
						tde4.setCurveAreaWidgetFOV (req,"CurveAreaWidgetY",frame_min,frame_max,y_y_min,y_y_max)

				else:
					import_status = False

				tde4.setCurveAreaWidgetCursorPosition(req,"CurveAreaWidgetX",tde4.getCurrentFrame(cam),1)
				tde4.setCurveAreaWidgetCursorPosition(req,"CurveAreaWidgetY",tde4.getCurrentFrame(cam),1)
			else:

				curvex_reloaded_temp = tde4.createCurve()
				curvey_reloaded_temp = tde4.createCurve()

				for i in selected_point_positions:
					tde4.createCurveKey(curvex_reloaded_temp,[i[0],i[1]])
					tde4.createCurveKey(curvey_reloaded_temp,[i[0],i[2]])


				if curvex_reloaded!= 0 :
					tde4.detachCurveAreaWidgetCurve(req,"CurveAreaWidgetX",curvex_reloaded)
					curvex_reloaded = 0
					tde4.setWidgetSensitiveFlag(req,"RestoreCurve",0)
					tde4.setWidgetSensitiveFlag(req,"ImportCurve",0)


				if curvey_reloaded!= 0 :
					tde4.detachCurveAreaWidgetCurve(req,"CurveAreaWidgetY",curvey_reloaded)
					curvey_reloaded = 0
					tde4.setWidgetSensitiveFlag(req,"RestoreCurve",0)
					tde4.setWidgetSensitiveFlag(req,"ImportCurve",0)

				pos_curvex = []
				pos_curvey = []	
				pos_curvex_reloaded_temp=[]
				pos_curvey_reloaded_temp=[]

				curvex_keys= tde4.getCurveKeyList(curvex,0)
				curvey_keys= tde4.getCurveKeyList(curvey,0)
				curvex_reloaded_temp_keys= tde4.getCurveKeyList(curvex_reloaded_temp,0)
				curvey_reloaded_temp_keys= tde4.getCurveKeyList(curvey_reloaded_temp,0)

				for k in curvex_keys:
					pos_curvex.append(tde4.getCurveKeyPosition(curvex,k))
				for k in curvey_keys:
					pos_curvey.append(tde4.getCurveKeyPosition(curvey,k))
				for k in curvex_reloaded_temp_keys:
					pos_curvex_reloaded_temp.append(tde4.getCurveKeyPosition(curvex_reloaded_temp,k))
				for k in curvey_reloaded_temp_keys:
					pos_curvey_reloaded_temp.append(tde4.getCurveKeyPosition(curvey_reloaded_temp,k))

				status = True	
				if len(pos_curvex) == len(pos_curvex_reloaded_temp):
					for i in range(len(pos_curvex) ):
						if pos_curvex[i] != pos_curvex_reloaded_temp[i]:
							status = False
							break
				else:
					status = False
				if status == False:
					curvex_reloaded = curvex_reloaded_temp
					tde4.attachCurveAreaWidgetCurve(req,"CurveAreaWidgetX",curvex_reloaded,0,1,0,0)
					tde4.setWidgetSensitiveFlag(req,"RestoreCurve",1)
					tde4.setWidgetSensitiveFlag(req,"ImportCurve",1)

				status = True	
				if len(pos_curvey) == len(pos_curvey_reloaded_temp):
					for i in range(len(pos_curvey) ):
						if pos_curvey[i] != pos_curvey_reloaded_temp[i]:
							status = False
							break
				else:
					status = False
				if status == False:
					curvey_reloaded = curvey_reloaded_temp
					tde4.attachCurveAreaWidgetCurve(req,"CurveAreaWidgetY",curvey_reloaded,0,1,0,0)
					tde4.setWidgetSensitiveFlag(req,"RestoreCurve",1)
					tde4.setWidgetSensitiveFlag(req,"ImportCurve",1)
				



	elif label == "FilterAllX":

		if float(tde4.getWidgetValue(req,"FilterAll_Min")) > float(tde4.getWidgetValue(req,"FilterAll_Max")) :
			tde4.postQuestionRequester("Error","Filter Min should be less than Filter Max","OK")
		else:
			if curvex_filtered_min!= 0 and curvex_filtered_max!= 0:
						tde4.detachCurveAreaWidgetCurve(req,"CurveAreaWidgetX",curvex_filtered_min)
						tde4.detachCurveAreaWidgetCurve(req,"CurveAreaWidgetX",curvex_filtered_max)
			curvex_filtered_min = tde4.createCurve()
			curvex_filtered_max = tde4.createCurve()
			curvex_y_to_filter = []	
			curvex_x = []
			curvex_keys= tde4.getCurveKeyList(curvex,0)
			for k in curvex_keys:
				pos = tde4.getCurveKeyPosition(curvex,k)
				curvex_y_to_filter.append(pos[1])
				curvex_x.append(pos[0])

			if len(curvex_y_to_filter) > 3:
				curvex_y_filtered_min = tde4.filter1D(curvex_y_to_filter,float(tde4.getWidgetValue(req,"FilterAll_Min")),"FOURIER_FILTER")
				curvex_y_filtered_max = tde4.filter1D(curvex_y_to_filter,float(tde4.getWidgetValue(req,"FilterAll_Max")),"FOURIER_FILTER")
			else:
				curvex_y_filtered_min = curvex_y_to_filter
				curvex_y_filtered_max = curvex_y_to_filter

			for i in range(len(curvex_x)):
				tde4.createCurveKey(curvex_filtered_min ,[curvex_x[i],curvex_y_filtered_min[i]])
				tde4.createCurveKey(curvex_filtered_max ,[curvex_x[i],curvex_y_filtered_max[i]])

			
			tde4.attachCurveAreaWidgetCurve(req,"CurveAreaWidgetX",curvex_filtered_min,1,0,0,0)
			tde4.attachCurveAreaWidgetCurve(req,"CurveAreaWidgetX",curvex_filtered_max,0,0,1,0)

	elif label == "FilterAllY":
		if float(tde4.getWidgetValue(req,"FilterAll_Min")) > float(tde4.getWidgetValue(req,"FilterAll_Max")) :
			tde4.postQuestionRequester("Error","Filter Min should be less than Filter Max","OK")
		else:
			if curvey_filtered_min!= 0 and curvey_filtered_max!= 0:
						tde4.detachCurveAreaWidgetCurve(req,"CurveAreaWidgetY",curvey_filtered_min)
						tde4.detachCurveAreaWidgetCurve(req,"CurveAreaWidgetY",curvey_filtered_max)

			curvey_filtered_min = tde4.createCurve()
			curvey_filtered_max = tde4.createCurve()
			curvey_y_to_filter = []	
			curvey_x = []	
			curvey_keys= tde4.getCurveKeyList(curvey,0)
			for k in curvey_keys:
				pos = tde4.getCurveKeyPosition(curvey,k)
				curvey_y_to_filter.append(pos[1])
				curvey_x.append(pos[0])


			if len(curvey_y_to_filter) > 3:
				curvey_y_filtered_min = tde4.filter1D(curvey_y_to_filter,float(tde4.getWidgetValue(req,"FilterAll_Min")),"FOURIER_FILTER")
				curvey_y_filtered_max = tde4.filter1D(curvey_y_to_filter,float(tde4.getWidgetValue(req,"FilterAll_Max")),"FOURIER_FILTER")
			else:
				curvey_y_filtered_min = curvey_y_to_filter
				curvey_y_filtered_max = curvey_y_to_filter


			for i in range(len(curvey_x)):
				tde4.createCurveKey(curvey_filtered_min ,[curvey_x[i],curvey_y_filtered_min[i]])
				tde4.createCurveKey(curvey_filtered_max ,[curvey_x[i],curvey_y_filtered_max[i]])
			
			tde4.attachCurveAreaWidgetCurve(req,"CurveAreaWidgetY",curvey_filtered_min,1,0,0,0)
			tde4.attachCurveAreaWidgetCurve(req,"CurveAreaWidgetY",curvey_filtered_max,0,0,1,0)

	elif label == "FilterAllXAdd":
		if float(tde4.getWidgetValue(req,"FilterAll_Min")) > float(tde4.getWidgetValue(req,"FilterAll_Max")) :
			tde4.postQuestionRequester("Error","Filter Min should be less than Filter Max","OK")
		else:
			curvex_x = []
			curvex_keys= tde4.getCurveKeyList(curvex,0)
			for k in curvex_keys:
				pos = tde4.getCurveKeyPosition(curvex,k)
				curvex_x.append(pos[0])
			adj_parameters_list.append(loaded_point + " " + "x" + " " + str(min(curvex_x)) + " " + str(max(curvex_x)) + " " + tde4.getWidgetValue(req,"FilterAll_Min") + " " + tde4.getWidgetValue(req,"FilterAll_Max") )
			list_adjs (req)
	elif label == "FilterAllYAdd":
		if float(tde4.getWidgetValue(req,"FilterAll_Min")) > float(tde4.getWidgetValue(req,"FilterAll_Max")) :
			tde4.postQuestionRequester("Error","Filter Min should be less than Filter Max","OK")
		else:
			curvey_x = []	
			curvey_keys= tde4.getCurveKeyList(curvey,0)
			for k in curvey_keys:
				pos = tde4.getCurveKeyPosition(curvey,k)
				curvey_x.append(pos[0])
			adj_parameters_list.append(loaded_point + " " + "y" + " " + str(min(curvey_x)) + " " + str(max(curvey_x)) + " " + tde4.getWidgetValue(req,"FilterAll_Min") + " " + tde4.getWidgetValue(req,"FilterAll_Max") )
			list_adjs (req)

	elif label == "FilterLocX":
		if float(tde4.getWidgetValue(req,"FilterLoc_Min")) > float(tde4.getWidgetValue(req,"FilterLoc_Max")) :
			tde4.postQuestionRequester("Error","Filter Min should be less than Filter Max","OK")
		elif float(tde4.getWidgetValue(req,"Range_Max")) - float(tde4.getWidgetValue(req,"Range_Min")) < 2:
			tde4.postQuestionRequester("Error","Range Min Max should be at least 3 frames","OK")
		else:
			if curvex_filtered_min!= 0 and curvex_filtered_max!= 0:
						tde4.detachCurveAreaWidgetCurve(req,"CurveAreaWidgetX",curvex_filtered_min)
						tde4.detachCurveAreaWidgetCurve(req,"CurveAreaWidgetX",curvex_filtered_max)
			curvex_filtered_min = tde4.createCurve()
			curvex_filtered_max = tde4.createCurve()
			curvex_y_to_filter = []	
			curvex_x = []
			curvex_keys= tde4.getCurveKeyList(curvex,0)
			for k in curvex_keys:
				pos = tde4.getCurveKeyPosition(curvex,k)
				if  pos[0] >= float(tde4.getWidgetValue(req,"Range_Min")) and pos[0] <= float(tde4.getWidgetValue(req,"Range_Max")):
					curvex_y_to_filter.append(pos[1])
					curvex_x.append(pos[0])

			if len(curvex_y_to_filter) > 3:
				curvex_y_filtered_min = tde4.filter1D(curvex_y_to_filter,float(tde4.getWidgetValue(req,"FilterLoc_Min")),"FOURIER_FILTER")
				curvex_y_filtered_max = tde4.filter1D(curvex_y_to_filter,float(tde4.getWidgetValue(req,"FilterLoc_Max")),"FOURIER_FILTER")
			else:
				curvex_y_filtered_min = curvex_y_to_filter
				curvex_y_filtered_max = curvex_y_to_filter

			for i in range(len(curvex_x)):
				tde4.createCurveKey(curvex_filtered_min ,[curvex_x[i],curvex_y_filtered_min[i]])
				tde4.createCurveKey(curvex_filtered_max ,[curvex_x[i],curvex_y_filtered_max[i]])

			
			tde4.attachCurveAreaWidgetCurve(req,"CurveAreaWidgetX",curvex_filtered_min,1,0,0,0)
			tde4.attachCurveAreaWidgetCurve(req,"CurveAreaWidgetX",curvex_filtered_max,0,0,1,0)

	elif label == "FilterLocY":
		if float(tde4.getWidgetValue(req,"FilterLoc_Min")) > float(tde4.getWidgetValue(req,"FilterLoc_Max")) :
			tde4.postQuestionRequester("Error","Filter Min should be less than Filter Max","OK")
		elif float(tde4.getWidgetValue(req,"Range_Max")) - float(tde4.getWidgetValue(req,"Range_Min")) < 2:
			tde4.postQuestionRequester("Error","Range Min Max should be at least 3 frames","OK")
		else:
			if curvey_filtered_min!= 0 and curvey_filtered_max!= 0:
						tde4.detachCurveAreaWidgetCurve(req,"CurveAreaWidgetY",curvey_filtered_min)
						tde4.detachCurveAreaWidgetCurve(req,"CurveAreaWidgetY",curvey_filtered_max)

			curvey_filtered_min = tde4.createCurve()
			curvey_filtered_max = tde4.createCurve()
			curvey_y_to_filter = []	
			curvey_x = []	
			curvey_keys= tde4.getCurveKeyList(curvey,0)
			for k in curvey_keys:
				pos = tde4.getCurveKeyPosition(curvey,k)
				if  pos[0] >= float(tde4.getWidgetValue(req,"Range_Min")) and pos[0] <= float(tde4.getWidgetValue(req,"Range_Max")):
					curvey_y_to_filter.append(pos[1])
					curvey_x.append(pos[0])

			if len(curvey_y_to_filter) > 3:
				curvey_y_filtered_min = tde4.filter1D(curvey_y_to_filter,float(tde4.getWidgetValue(req,"FilterLoc_Min")),"FOURIER_FILTER")
				curvey_y_filtered_max = tde4.filter1D(curvey_y_to_filter,float(tde4.getWidgetValue(req,"FilterLoc_Max")),"FOURIER_FILTER")
			else:
				curvey_y_filtered_min = curvey_y_to_filter
				curvey_y_filtered_max = curvey_y_to_filter

			for i in range(len(curvey_x)):
				tde4.createCurveKey(curvey_filtered_min ,[curvey_x[i],curvey_y_filtered_min[i]])
				tde4.createCurveKey(curvey_filtered_max ,[curvey_x[i],curvey_y_filtered_max[i]])
			
			tde4.attachCurveAreaWidgetCurve(req,"CurveAreaWidgetY",curvey_filtered_min,1,0,0,0)
			tde4.attachCurveAreaWidgetCurve(req,"CurveAreaWidgetY",curvey_filtered_max,0,0,1,0)

	elif label == "FilterLocXAdd":
		if float(tde4.getWidgetValue(req,"FilterLoc_Min")) > float(tde4.getWidgetValue(req,"FilterLoc_Max")) :
			tde4.postQuestionRequester("Error","Filter Min should be less than Filter Max","OK")
		elif float(tde4.getWidgetValue(req,"Range_Max")) - float(tde4.getWidgetValue(req,"Range_Min")) < 2:
			tde4.postQuestionRequester("Error","Range Min Max should be at least 3 frames","OK")
		else:
			adj_parameters_list.append(loaded_point + " " + "x" + " " + tde4.getWidgetValue(req,"Range_Min") + " " + tde4.getWidgetValue(req,"Range_Max") + " " + tde4.getWidgetValue(req,"FilterLoc_Min") + " " + tde4.getWidgetValue(req,"FilterLoc_Max") )
			list_adjs (req)
	elif label == "FilterLocYAdd":
		if float(tde4.getWidgetValue(req,"FilterLoc_Min")) > float(tde4.getWidgetValue(req,"FilterLoc_Max")) :
			tde4.postQuestionRequester("Error","Filter Min should be less than Filter Max","OK")
		elif float(tde4.getWidgetValue(req,"Range_Max")) - float(tde4.getWidgetValue(req,"Range_Min")) < 2:
			tde4.postQuestionRequester("Error","Range Min Max should be at least 3 frames","OK")
		else:
			adj_parameters_list.append(loaded_point + " " + "y" + " " + tde4.getWidgetValue(req,"Range_Min") + " " +tde4.getWidgetValue(req,"Range_Max") + " " + tde4.getWidgetValue(req,"FilterLoc_Min") + " " + tde4.getWidgetValue(req,"FilterLoc_Max") )
			list_adjs (req)
	elif label == "DeleteAdj":
		for i in range(tde4.getListWidgetNoItems(req,"ListAdj") -1 , -1 , -1   ):
			if tde4.getListWidgetItemSelectionFlag(req,"ListAdj",i) == 1:
				userdata = tde4.getListWidgetItemUserdata(req,"ListAdj",i)
				tde4.removeListWidgetItem(req,"ListAdj",i)
				del adj_parameters_list [userdata]
	elif label == "DeleteAdjAll":
		tde4.removeAllListWidgetItems(req,"ListAdj")
		adj_parameters_list = []
	elif label == "SendAdj":

		for i in range(tde4.getListWidgetNoItems(req,"ListAdj")):
			if tde4.getListWidgetItemSelectionFlag(req,"ListAdj",i) == 1:
				userdata = tde4.getListWidgetItemUserdata(req,"ListAdj",i)
				param = adj_parameters_list [userdata].split()
				pointid_curve_start_end = str(param[0]) + " " + str(param[1]) + " " + str(param[2]) + " " + str(param[3])
				filter_start =  param[4]
				filter_end =  param[5]
				tde4.createParameterAdjustScript(script_base + adj_script_name,"filter adj " + pointid_curve_start_end,pointid_curve_start_end ,float(filter_start),float(filter_end))
	elif label == "SendAdjAll":
		for i in range(tde4.getListWidgetNoItems(req,"ListAdj")):
			userdata = tde4.getListWidgetItemUserdata(req,"ListAdj",i)
			param = adj_parameters_list [userdata].split()
			pointid_curve_start_end = str(param[0]) + " " + str(param[1]) + " " + str(param[2]) + " " + str(param[3])
			filter_start =  param[4]
			filter_end =  param[5]
			tde4.createParameterAdjustScript(script_base + adj_script_name,"filter adj " + pointid_curve_start_end,pointid_curve_start_end ,float(filter_start),float(filter_end))

	elif label == "RestoreCurve":
		cam = tde4.getCurrentCamera()
		current_point_group = tde4.getCurrentPGroup()
		playback = tde4.getCameraPlaybackRange(cam)
		firstframe = playback[0]
		lastframe = playback[1]


		pos_x = []
		pos_y = []
		curvex_keys= tde4.getCurveKeyList(curvex,0)
		for k in curvex_keys:
			pos_x.append(tde4.getCurveKeyPosition(curvex,k))
		curvey_keys= tde4.getCurveKeyList(curvey,0)
		for k in curvey_keys:
			pos_y.append(tde4.getCurveKeyPosition(curvey,k))

		for i in range(len(pos_x)):
			tde4.setPointPosition2D(current_point_group,loaded_point,cam,int(pos_x[i][0]),[pos_x[i][1],pos_y[i][1]]) 

		callback(req,"ImportCurve","")
		tde4.setWidgetSensitiveFlag(req,"RestoreCurve",0)
		tde4.setWidgetSensitiveFlag(req,"ImportCurve",0)

	elif label == "ImportCurve":
		loaded_point = ""
		import_status = True
		callback(req,"LoadCurve","")
		tde4.setWidgetSensitiveFlag(req,"RestoreCurve",0)
		tde4.setWidgetSensitiveFlag(req,"ImportCurve",0)




editor = tde4.createCustomRequester()


tde4.addCurveAreaWidget(editor,"CurveAreaWidgetX","X",0)
tde4.setWidgetAttachModes(editor,"CurveAreaWidgetX","ATTACH_WINDOW","ATTACH_WINDOW","ATTACH_WINDOW","ATTACH_WINDOW")
tde4.setWidgetOffsets(editor,"CurveAreaWidgetX",25,280,0,450)

tde4.addCurveAreaWidget(editor,"CurveAreaWidgetY","Y",0)
tde4.setWidgetLinks(editor,"CurveAreaWidgetY","","","CurveAreaWidgetX","")
tde4.setWidgetAttachModes(editor,"CurveAreaWidgetY","ATTACH_WINDOW","ATTACH_WINDOW","ATTACH_WIDGET","ATTACH_WINDOW")
tde4.setWidgetOffsets(editor,"CurveAreaWidgetY",25,280,5,100)

tde4.addListWidget(editor,"ListAdj","",1,0)
tde4.setWidgetLinks(editor,"ListAdj","CurveAreaWidgetX","","","")
tde4.setWidgetAttachModes(editor,"ListAdj","ATTACH_WIDGET","ATTACH_WINDOW","ATTACH_WINDOW","ATTACH_WINDOW")
tde4.setWidgetOffsets(editor,"ListAdj",5,5,0,155)


tde4.addButtonWidget(editor,"DeleteAdj","Delete",100,10)
tde4.setWidgetSize(editor,"DeleteAdj",50,50)
tde4.setWidgetLinks(editor,"DeleteAdj","CurveAreaWidgetY","","ListAdj","")
tde4.setWidgetAttachModes(editor,"DeleteAdj","ATTACH_WIDGET","ATTACH_AS_IS","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetCallbackFunction(editor,"DeleteAdj","callback")

tde4.addButtonWidget(editor,"DeleteAdjAll","Delete All",100,10)
tde4.setWidgetSize(editor,"DeleteAdjAll",50,50)
tde4.setWidgetLinks(editor,"DeleteAdjAll","DeleteAdj","","ListAdj","")
tde4.setWidgetAttachModes(editor,"DeleteAdjAll","ATTACH_WIDGET","ATTACH_AS_IS","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetCallbackFunction(editor,"DeleteAdjAll","callback")

tde4.addButtonWidget(editor,"SendAdj","Send Adj",100,10)
tde4.setWidgetSize(editor,"SendAdj",50,50)
tde4.setWidgetLinks(editor,"SendAdj","DeleteAdjAll","","ListAdj","")
tde4.setWidgetAttachModes(editor,"SendAdj","ATTACH_WIDGET","ATTACH_AS_IS","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetCallbackFunction(editor,"SendAdj","callback")

tde4.addButtonWidget(editor,"SendAdjAll","Send Adj All",100,10)
tde4.setWidgetSize(editor,"SendAdjAll",80,50)
tde4.setWidgetLinks(editor,"SendAdjAll","SendAdj","","ListAdj","")
tde4.setWidgetAttachModes(editor,"SendAdjAll","ATTACH_WIDGET","ATTACH_AS_IS","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetCallbackFunction(editor,"SendAdjAll","callback")




tde4.addButtonWidget(editor,"LoadCurve","LoadCurve",100,10)
tde4.setWidgetSize(editor,"LoadCurve",100,50)
tde4.setWidgetLinks(editor,"LoadCurve","","","CurveAreaWidgetY","")
tde4.setWidgetAttachModes(editor,"LoadCurve","ATTACH_WINDOW","ATTACH_AS_IS","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetCallbackFunction(editor,"LoadCurve","callback")

tde4.addButtonWidget(editor,"RestoreCurve","Restore",100,10)
tde4.setWidgetSize(editor,"RestoreCurve",100,50)
tde4.setWidgetLinks(editor,"RestoreCurve","LoadCurve","","CurveAreaWidgetY","")
tde4.setWidgetAttachModes(editor,"RestoreCurve","ATTACH_WIDGET","ATTACH_AS_IS","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetSensitiveFlag(editor,"RestoreCurve",0)
tde4.setWidgetCallbackFunction(editor,"RestoreCurve","callback")

tde4.addButtonWidget(editor,"ImportCurve","Import",100,10)
tde4.setWidgetSize(editor,"ImportCurve",100,50)
tde4.setWidgetLinks(editor,"ImportCurve","RestoreCurve","","CurveAreaWidgetY","")
tde4.setWidgetAttachModes(editor,"ImportCurve","ATTACH_WIDGET","ATTACH_AS_IS","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetSensitiveFlag(editor,"ImportCurve",0)
tde4.setWidgetCallbackFunction(editor,"ImportCurve","callback")


tde4.addTextFieldWidget(editor,"FilterAll_Min","","0.1")
tde4.setWidgetLinks(editor,"FilterAll_Min","ImportCurve","ImportCurve","CurveAreaWidgetY","")
tde4.setWidgetAttachModes(editor,"FilterAll_Min","ATTACH_WIDGET","ATTACH_OPPOSITE_WIDGET","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetSize(editor,"FilterAll_Min",50,50)
tde4.setWidgetOffsets(editor,"FilterAll_Min",50,-80,5,0)

tde4.addTextFieldWidget(editor,"FilterAll_Max","","2")
tde4.setWidgetLinks(editor,"FilterAll_Max","FilterAll_Min","FilterAll_Min","CurveAreaWidgetY","")
tde4.setWidgetAttachModes(editor,"FilterAll_Max","ATTACH_WIDGET","ATTACH_OPPOSITE_WIDGET","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetSize(editor,"FilterAll_Max",50,50)
tde4.setWidgetOffsets(editor,"FilterAll_Max",10,-40,5,0)


tde4.addButtonWidget(editor,"FilterAllX","Global Preview X",100,10)
tde4.setWidgetSize(editor,"FilterAllX",150,50)
tde4.setWidgetLinks(editor,"FilterAllX","FilterAll_Max","","CurveAreaWidgetY","")
tde4.setWidgetAttachModes(editor,"FilterAllX","ATTACH_WIDGET","ATTACH_AS_IS","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetSensitiveFlag(editor,"FilterAllX",0)
tde4.setWidgetCallbackFunction(editor,"FilterAllX","callback")

tde4.addButtonWidget(editor,"FilterAllY","Y",100,10)
tde4.setWidgetSize(editor,"FilterAllY",20,50)
tde4.setWidgetLinks(editor,"FilterAllY","FilterAllX","","CurveAreaWidgetY","")
tde4.setWidgetAttachModes(editor,"FilterAllY","ATTACH_WIDGET","ATTACH_AS_IS","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetSensitiveFlag(editor,"FilterAllY",0)
tde4.setWidgetCallbackFunction(editor,"FilterAllY","callback")

tde4.addButtonWidget(editor,"FilterAllXAdd","+X",100,10)
tde4.setWidgetSize(editor,"FilterAllXAdd",25,50)
tde4.setWidgetLinks(editor,"FilterAllXAdd","FilterAllY","","CurveAreaWidgetY","")
tde4.setWidgetAttachModes(editor,"FilterAllXAdd","ATTACH_WIDGET","ATTACH_AS_IS","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetSensitiveFlag(editor,"FilterAllXAdd",0)
tde4.setWidgetCallbackFunction(editor,"FilterAllXAdd","callback")

tde4.addButtonWidget(editor,"FilterAllYAdd","+Y",100,10)
tde4.setWidgetSize(editor,"FilterAllYAdd",25,50)
tde4.setWidgetLinks(editor,"FilterAllYAdd","FilterAllXAdd","","CurveAreaWidgetY","")
tde4.setWidgetAttachModes(editor,"FilterAllYAdd","ATTACH_WIDGET","ATTACH_AS_IS","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetSensitiveFlag(editor,"FilterAllYAdd",0)
tde4.setWidgetCallbackFunction(editor,"FilterAllYAdd","callback")




tde4.addTextFieldWidget(editor,"FilterLoc_Min","","0.1")
tde4.setWidgetLinks(editor,"FilterLoc_Min","FilterAllYAdd","FilterAllYAdd","CurveAreaWidgetY","")
tde4.setWidgetAttachModes(editor,"FilterLoc_Min","ATTACH_WIDGET","ATTACH_OPPOSITE_WIDGET","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetSize(editor,"FilterLoc_Min",50,50)
tde4.setWidgetOffsets(editor,"FilterLoc_Min",60,-90,5,0)

tde4.addTextFieldWidget(editor,"FilterLoc_Max","","2")
tde4.setWidgetLinks(editor,"FilterLoc_Max","FilterLoc_Min","FilterLoc_Min","CurveAreaWidgetY","")
tde4.setWidgetAttachModes(editor,"FilterLoc_Max","ATTACH_WIDGET","ATTACH_OPPOSITE_WIDGET","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetSize(editor,"FilterLoc_Max",50,50)
tde4.setWidgetOffsets(editor,"FilterLoc_Max",10,-40,5,0)


tde4.addButtonWidget(editor,"FilterLocX","Local Preview X",100,10)
tde4.setWidgetSize(editor,"FilterLocX",150,50)
tde4.setWidgetLinks(editor,"FilterLocX","FilterLoc_Max","","CurveAreaWidgetY","")
tde4.setWidgetAttachModes(editor,"FilterLocX","ATTACH_WIDGET","ATTACH_AS_IS","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetSensitiveFlag(editor,"FilterLocX",0)
tde4.setWidgetCallbackFunction(editor,"FilterLocX","callback")

tde4.addButtonWidget(editor,"FilterLocY","Y",100,10)
tde4.setWidgetSize(editor,"FilterLocY",20,50)
tde4.setWidgetLinks(editor,"FilterLocY","FilterLocX","","CurveAreaWidgetY","")
tde4.setWidgetAttachModes(editor,"FilterLocY","ATTACH_WIDGET","ATTACH_AS_IS","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetSensitiveFlag(editor,"FilterLocY",0)
tde4.setWidgetCallbackFunction(editor,"FilterLocY","callback")

tde4.addButtonWidget(editor,"FilterLocXAdd","+X",100,10)
tde4.setWidgetSize(editor,"FilterLocXAdd",25,50)
tde4.setWidgetLinks(editor,"FilterLocXAdd","FilterLocY","","CurveAreaWidgetY","")
tde4.setWidgetAttachModes(editor,"FilterLocXAdd","ATTACH_WIDGET","ATTACH_AS_IS","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetSensitiveFlag(editor,"FilterLocXAdd",0)
tde4.setWidgetCallbackFunction(editor,"FilterLocXAdd","callback")

tde4.addButtonWidget(editor,"FilterLocYAdd","+Y",100,10)
tde4.setWidgetSize(editor,"FilterLocYAdd",25,50)
tde4.setWidgetLinks(editor,"FilterLocYAdd","FilterLocXAdd","","CurveAreaWidgetY","")
tde4.setWidgetAttachModes(editor,"FilterLocYAdd","ATTACH_WIDGET","ATTACH_AS_IS","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetSensitiveFlag(editor,"FilterLocYAdd",0)
tde4.setWidgetCallbackFunction(editor,"FilterLocYAdd","callback")


tde4.addTextFieldWidget(editor,"Range_Min","","1")
tde4.setWidgetLinks(editor,"Range_Min","FilterLocYAdd","FilterLocYAdd","CurveAreaWidgetY","")
tde4.setWidgetAttachModes(editor,"Range_Min","ATTACH_WIDGET","ATTACH_OPPOSITE_WIDGET","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetSize(editor,"Range_Min",50,50)
tde4.setWidgetOffsets(editor,"Range_Min",10,-40,5,0)

tde4.addTextFieldWidget(editor,"Range_Max","","3")
tde4.setWidgetLinks(editor,"Range_Max","Range_Min","Range_Min","CurveAreaWidgetY","")
tde4.setWidgetAttachModes(editor,"Range_Max","ATTACH_WIDGET","ATTACH_OPPOSITE_WIDGET","ATTACH_WIDGET","ATTACH_AS_IS")
tde4.setWidgetSize(editor,"Range_Max",50,50)
tde4.setWidgetOffsets(editor,"Range_Max",10,-40,5,0)





tde4.addTextFieldWidget(editor,"PointName","Loaded Point Name","")
tde4.setWidgetSensitiveFlag(editor,"PointName",0)
tde4.setWidgetAttachModes(editor,"PointName","ATTACH_WINDOW","ATTACH_WINDOW","ATTACH_NONE","ATTACH_WINDOW")
tde4.setWidgetSize(editor,"PointName",20,20)
tde4.setWidgetOffsets(editor,"PointName",150,400,0,10)


tde4.addTextFieldWidget(editor,"PointID","Loaded Point ID","")
tde4.setWidgetSensitiveFlag(editor,"PointID",0)
tde4.setWidgetLinks(editor,"PointID","PointName","","","")
tde4.setWidgetAttachModes(editor,"PointID","ATTACH_WIDGET","ATTACH_WINDOW","ATTACH_NONE","ATTACH_WINDOW")
tde4.setWidgetSize(editor,"PointID",20,20)
tde4.setWidgetOffsets(editor,"PointID",150,50,0,10)

'''

tde4.addScaleWidget(editor,"scale1","scale1label","INT",-100,100,0,1)
tde4.setWidgetCallbackFunction(editor,"scale1","callback_scale")

tde4.addTextFieldWidget(editor,"text","textlabel","0 0 0 0")

'''



tde4.postCustomRequesterAndContinue (editor,"2D Curve Local Filter Editor",editor_width,editor_height,"callback_editor")
