#
#
# 3DE4.script.hide: true
# 3DE4.script.startup: false
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#


import math

#init
current_value =  tde4.getParameterAdjustCurrentValue()
get_custom_data = tde4.getParameterAdjustCustomParameters()
running = tde4.getParameterAdjustPhaseRunningFlag()

# script specific
params = get_custom_data.split()
point_id = params[0]
curve = params[1]
start = math.floor(float(params[2]))
end = math.floor(float(params[3]))


cam = tde4.getCurrentCamera()
current_point_group = tde4.getCurrentPGroup()
playback = tde4.getCameraPlaybackRange(cam)
firstframe = playback[0]
lastframe = playback[1]



selected_point_positions = []
points_to_filter = []
points_filtered = []
for frame in range (firstframe, lastframe + 1):
	if frame >= start and  frame <= end and tde4.isPointPos2DValid(current_point_group,point_id,cam,frame):
		selected_point_position = tde4.getPointPosition2D(current_point_group,point_id,cam,frame)
		selected_point_positions.append([frame,selected_point_position[0],selected_point_position[1]])
		if curve =="x":
			points_to_filter.append(selected_point_position[0])
		else:
			points_to_filter.append(selected_point_position[1])

if len(points_to_filter) >= 3 :
	points_filtered = tde4.filter1D(points_to_filter,current_value,"FOURIER_FILTER")
else:
	points_filtered = points_to_filter

if (running):
	#calculating
	for i in range(len(selected_point_positions)):
		if curve =="x":
			tde4.applyParameterAdjustDataModification("project.pgroups.@" + current_point_group +".points.@" + point_id + ".cameras.@" + cam +".frame." + str(selected_point_positions[i][0]) + ".position_2d[0]",points_filtered[i])
		else:
			tde4.applyParameterAdjustDataModification("project.pgroups.@" + current_point_group +".points.@" + point_id + ".cameras.@" + cam +".frame." + str(selected_point_positions[i][0]) + ".position_2d[1]",points_filtered[i])
else:
	#transfer_to_3de
	for i in range(len(selected_point_positions)):
		if curve =="x":
			tde4.setPointPosition2D(current_point_group,point_id,cam, selected_point_positions[i][0], [ points_filtered[i],selected_point_positions[i][2]]  )
		else:
			tde4.setPointPosition2D(current_point_group,point_id,cam, selected_point_positions[i][0], [ selected_point_positions[i][1],points_filtered[i]] )



