#
#
# 3DE4.script.name:	Backup 2D Curves
#
# 3DE4.script.version:	v1.0
#
# 3DE4.script.gui:	Manual Tracking::Edit
#
# 3DE4.script.comment:	Backup 2D Curves in memory
# 3DE4.script.gui.button:	Manual Tracking::Backup, align-bottom-right, 120, 20
#
#
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#

import xg_backup_restore_2d_curves
xg_backup_restore_2d_curves = reload(xg_backup_restore_2d_curves)

xg_backup_restore_2d_curves.backup () 







