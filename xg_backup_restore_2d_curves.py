# 3DE4.script.hide: true
# 3DE4.script.startup: false
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#

import tde4

global backup_2d_curves
try:
		backup_2d_curves
except :
		backup_2d_curves = {}

cam = tde4.getCurrentCamera()
current_point_group = tde4.getCurrentPGroup()
selected_points = tde4.getPointList (current_point_group,1)
playback = tde4.getCameraPlaybackRange(cam)
firstframe = playback[0]
lastframe = playback[1]

def backup () :
	for point in selected_points:
		backup_2d_curves[point] = []
		for frame in range (firstframe, lastframe + 1):
			if tde4.isPointPos2DValid(current_point_group,point,cam,frame):
				selected_point_position = tde4.getPointPosition2D(current_point_group,point,cam,frame)
				selected_point_position_status = tde4.getPointStatus2D(current_point_group,point,cam,frame)
				backup_2d_curves[point].append([frame,selected_point_position,selected_point_position_status])

def restore ():
	for point in selected_points:
		if point in backup_2d_curves:
			tde4.deletePointCurve2D (current_point_group,point,cam)
			for i in backup_2d_curves[point]:
				tde4.setPointPosition2D(current_point_group,point,cam,i[0],i[1])
			for i in backup_2d_curves[point]:
				tde4.setPointStatus2D(current_point_group,point,cam,i[0],i[2])
				
				









