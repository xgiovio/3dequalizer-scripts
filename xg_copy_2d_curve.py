#
#
# 3DE4.script.name:	Copy 2D Curve
#
# 3DE4.script.version:		v1.0
#
# 3DE4.script.comment:	Copy 2D Curve in memory
#
# 3DE4.script.gui:	Manual Tracking Controls::Edit
# 3DE4.script.gui.button:	Manual Tracking Controls::Copy, align-bottom-left, 100, 20


# 3DE4.script.hide: false
# 3DE4.script.startup: false
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#



import xg_copy_paste_2d_curves
xg_copy_paste_2d_curves = reload(xg_copy_paste_2d_curves)

xg_copy_paste_2d_curves.copy ()
