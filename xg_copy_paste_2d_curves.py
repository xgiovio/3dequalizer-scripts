# 3DE4.script.hide: true
# 3DE4.script.startup: false
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#

import tde4

global copied_2d_curve
try:
		copied_2d_curve
except :
		copied_2d_curve = {}

cam = tde4.getCurrentCamera()
current_point_group = tde4.getCurrentPGroup()
selected_points = tde4.getPointList (current_point_group,1)
playback = tde4.getCameraPlaybackRange(cam)
firstframe = playback[0]
lastframe = playback[1]

def copy () :
	if len(selected_points) != 1 :
		tde4.postQuestionRequester("Error","You need to select 1 2D point","OK")
	else:
		for point in selected_points:
			for frame in range (firstframe, lastframe + 1):
				if tde4.isPointPos2DValid(current_point_group,point,cam,frame):
					selected_point_position = tde4.getPointPosition2D(current_point_group,point,cam,frame)
					copied_2d_curve[frame] = selected_point_position



def paste (adapt_to_keyframes = False, inside_only = False):
	if len(selected_points) != 1 :
		tde4.postQuestionRequester("Error","You need to select 1 2D point","OK")
	else:
		for point in selected_points:
			if adapt_to_keyframes :
				k_pos = {}
				for frame in range (firstframe, lastframe + 1):
					point_status = tde4.getPointStatus2D(current_point_group,point,cam,frame)
					if point_status == "POINT_KEYFRAME" or point_status == "POINT_KEYFRAME_END":
						pos = tde4.getPointPosition2D(current_point_group,point,cam,frame)
						k_pos[frame]= pos

			tde4.deletePointCurve2D (current_point_group,point,cam)

			if not adapt_to_keyframes or len(k_pos) == 0 :
				for frame in copied_2d_curve:
					tde4.setPointPosition2D(current_point_group,point,cam,frame,copied_2d_curve[frame])
			else:
				k_pos_to_delete = []
				for frame in k_pos:
					if frame in copied_2d_curve:
						copied_2d_curve_x = copied_2d_curve[frame][0]
						copied_2d_curve_y = copied_2d_curve[frame][1]
						k_pos_x = k_pos[frame][0]
						k_pos_y =  k_pos[frame][1]
						offset_x = k_pos_x - copied_2d_curve_x
						offset_y = k_pos_y - copied_2d_curve_y
						k_pos[frame] = [offset_x,offset_y]
					else :
						k_pos_to_delete.append(frame)
				for frame in k_pos_to_delete:
					del k_pos[frame]


				if len(k_pos) > 0: 
					#extend keyframes to source curve range
					k_pos_extended = {}

					min_frame_k_pos = min(k_pos)
					max_frame_k_pos = max(k_pos)

					min_frame_copied_2d_curve = min(copied_2d_curve)
					max_frame_copied_2d_curve = max(copied_2d_curve)

					if min_frame_k_pos != min_frame_copied_2d_curve and not inside_only:
						k_pos_extended[min_frame_copied_2d_curve] = k_pos[min_frame_k_pos] 
					for frame in k_pos:
						k_pos_extended[frame] = k_pos[frame]
					if max_frame_k_pos != max_frame_copied_2d_curve and not inside_only:
						k_pos_extended[max_frame_copied_2d_curve] = k_pos[max_frame_k_pos] 


					k_pos_extended_keys = k_pos_extended.keys()
					k_pos_extended_keys.sort()

					for i in range(len(k_pos_extended_keys) - 1):
						print 
						f0 = k_pos_extended_keys[i]
						f1 = k_pos_extended_keys[i + 1]
						xo0 = k_pos_extended[k_pos_extended_keys[i]][0]
						yo0 = k_pos_extended[k_pos_extended_keys[i]][1]
						xo1 = k_pos_extended[k_pos_extended_keys[i+ 1]][0]
						yo1 = k_pos_extended[k_pos_extended_keys[i + 1]][1]
						# linear deform step
						for f in range(f0, f1):
							xoI = (xo0 + ((f - f0) * ((xo1 - xo0) / (f1 - f0))))
							yoI = (yo0 + ((f - f0) * ((yo1 - yo0) / (f1 - f0))))
							copied_2d_curve_x = copied_2d_curve[f][0]
							copied_2d_curve_y = copied_2d_curve[f][1]
							tde4.setPointPosition2D(current_point_group,point,cam,f,[copied_2d_curve_x + xoI,  copied_2d_curve_y + yoI ] )
					#lastkeyframe
					copied_2d_curve_x = copied_2d_curve[f1][0]
					copied_2d_curve_y = copied_2d_curve[f1][1]
					tde4.setPointPosition2D(current_point_group,point,cam,f1,[copied_2d_curve_x + xo1,  copied_2d_curve_y + yo1 ] )








