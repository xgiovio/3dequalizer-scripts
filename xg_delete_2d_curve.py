#
#
# 3DE4.script.name:	Delete 2d Curve
#
# 3DE4.script.version:	v1.0
#
# 3DE4.script.gui:	Manual Tracking::Edit
# 3DE4.script.gui:	Object Browser::Context Menu Point
#
# 3DE4.script.comment:	Delete 2d curve
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#

cam = tde4.getCurrentCamera()
current_point_group = tde4.getCurrentPGroup()
selected_points = tde4.getPointList (current_point_group,1)

for point in selected_points:
	tde4.deletePointCurve2D (current_point_group,point,cam)








