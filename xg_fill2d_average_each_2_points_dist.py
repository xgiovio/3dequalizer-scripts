#
#
# 3DE4.script.name:	Average each 2 points 2D with Dist
#
# 3DE4.script.version:		v1.0
#
# 3DE4.script.comment:	Average each 2 2D points  with Distortion
#
# 3DE4.script.gui:	Manual Tracking Controls::Fill2D
# 3DE4.script.hide: false
# 3DE4.script.startup: false
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#

import xg_fill2d
xg_fill2d = reload(xg_fill2d)

xg_fill2d.average_each_2_points (dist = True)