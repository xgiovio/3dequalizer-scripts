# 3DE4.script.hide: true
# 3DE4.script.startup: false
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#

import itertools
import tde4


def average_each_2_points (reelin= False, distortion = False, visible_only = False):
	cam = tde4.getCurrentCamera()
	current_point_group = tde4.getCurrentPGroup()
	selected_points = tde4.getPointList (current_point_group,1)
	playback = tde4.getCameraPlaybackRange(cam)
	firstframe = playback[0]
	lastframe = playback[1]
	if (len(selected_points) < 2):
		tde4.postQuestionRequester("Info","You need to select 2 or more 2D points with 3D Position","OK")
	else:
		tde4.postProgressRequesterAndContinue("Calculating","Computing Points",100,"Cancel")
		couples_selected_points = list(itertools.combinations(selected_points,2))
		counter = 0
		for i in couples_selected_points:
			counter+=1
			percentage = (100 * counter) / len(couples_selected_points)
			aname = tde4.getPointName(current_point_group,i[0])
			bname = tde4.getPointName(current_point_group,i[1])
			status = tde4.updateProgressRequester(percentage,str(percentage) + " % -" + " Point AVG3D_" + aname + "_" + bname)
			if tde4.getPointCalculated3DStatus(current_point_group,i[0]) != "NOT_CALCULATED" and tde4.getPointCalculated3DStatus(current_point_group,i[1])  != "NOT_CALCULATED" :
				avg = tde4.createPoint (current_point_group)
				tde4.setPointName(current_point_group,avg, "AVG3D_" + aname + "_" + bname)
				
				apos = tde4.getPointCalcPosition3D (current_point_group,i[0])
				aposx = apos[0]
				aposy = apos[1]
				aposz = apos[2]
				bpos = tde4.getPointCalcPosition3D(current_point_group,i[1])
			 	bposx = bpos[0]
			 	bposy = bpos[1]
			 	bposz = bpos[2]


				avgpos = [ aposx + ( (bposx - aposx) / 2 ) , aposy + ( (bposy - aposy) / 2 ), aposz + ( (bposz - aposz) / 2 ) ]

				tde4.setPointCalcPosition3D (current_point_group,avg,avgpos)
				tde4.setPointCalculated3D(current_point_group,avg,1)
				tde4.setPointCalculated3DStatus(current_point_group,avg,"TRIANGULATED")
				if reelin :
						for frame in range (firstframe, lastframe + 1):
							if distortion:
								pos = tde4.calcPointBackProjection2D(current_point_group,avg,cam,frame,1)
							else:
								pos = tde4.calcPointBackProjection2D(current_point_group,avg,cam,frame,0)
							if visible_only :
								if pos[0] >= 0 and pos[0] <= 1 and  pos[1] >= 0 and pos[1] <= 1:
									tde4.setPointPosition2D(current_point_group,avg,cam,frame,pos)
							else:
								tde4.setPointPosition2D(current_point_group,avg,cam,frame,pos)
			
			if status != -1:
				break