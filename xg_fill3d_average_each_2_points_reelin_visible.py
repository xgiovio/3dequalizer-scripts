#
#
# 3DE4.script.name:	Avg each 2 points 3D + Reel in Visible
#
# 3DE4.script.version:		v1.0
#
# 3DE4.script.comment:	Average each 2 3d points and reel in in visibile frames area
#
# 3DE4.script.gui:	Manual Tracking Controls::Fill3D
# 3DE4.script.gui:	Manual Tracking Controls::Reconstruction
# 3DE4.script.hide: false
# 3DE4.script.startup: false
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#

import xg_fill3d
xg_fill3d = reload(xg_fill3d)


xg_fill3d.average_each_2_points (reelin = True, visible_only = True)