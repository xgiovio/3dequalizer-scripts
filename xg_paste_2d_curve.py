#
#
# 3DE4.script.name:	Paste 2D Curve
#
# 3DE4.script.version:		v1.0
#
# 3DE4.script.comment:	Paste copied curve to selected point. Overwrite present curve	
#
# 3DE4.script.gui:	Manual Tracking Controls::Edit
# 3DE4.script.gui.button:	Manual Tracking Controls::Paste, align-bottom-left, 100, 20


# 3DE4.script.hide: false
# 3DE4.script.startup: false
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#



import xg_copy_paste_2d_curves
xg_copy_paste_2d_curves = reload(xg_copy_paste_2d_curves)

xg_copy_paste_2d_curves.paste ()
