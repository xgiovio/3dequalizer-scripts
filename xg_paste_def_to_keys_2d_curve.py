#
#
# 3DE4.script.name:	Paste and Deform 2D Curve to Keys 
#
# 3DE4.script.version:		v1.0
#
# 3DE4.script.comment:	Paste copied curve to new curves deforming using actual keyframes on new curve.
#
# 3DE4.script.gui:	Manual Tracking Controls::Edit
# 3DE4.script.gui.button:	Manual Tracking Controls::Paste and Deform to Keys, align-bottom-left, 150, 20


# 3DE4.script.hide: false
# 3DE4.script.startup: false
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#



import xg_copy_paste_2d_curves
xg_copy_paste_2d_curves = reload(xg_copy_paste_2d_curves)

xg_copy_paste_2d_curves.paste (adapt_to_keyframes = True)
