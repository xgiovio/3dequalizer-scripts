#
#
# 3DE4.script.name:	Delete Non Keyframes and Triangulate
#
# 3DE4.script.version:		v1.0
#
# 3DE4.script.comment:	Delete Non Keyframes,Triangulate
#
# 3DE4.script.gui:	Manual Tracking Controls::Reconstruction
# 3DE4.script.gui.button:	Manual Tracking Controls::Del Non Keyframes + Trian, align-bottom-left, 120, 20


# 3DE4.script.hide: false
# 3DE4.script.startup: false
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#



import xg_reconstruct_tool
xg_reconstruct_tool = reload(xg_reconstruct_tool)

xg_reconstruct_tool.operations (del_non_keyframes = True, triangulate = True)
