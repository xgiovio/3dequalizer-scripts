#
#
# 3DE4.script.name:	Delete Non Keyframes,Triangulate,Reel Deform
#
# 3DE4.script.version:		v1.0
#
# 3DE4.script.comment:	Delete Non Keyframes,Triangulate and Reel in  with distortion
# 3DE4.script.comment:	and deform the track on previous keyframes. Recalculate from scratch needed after this command.
#
# 3DE4.script.gui:	Manual Tracking Controls::Reconstruction


# 3DE4.script.hide: false
# 3DE4.script.startup: false
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#



import xg_reconstruct_tool
xg_reconstruct_tool = reload(xg_reconstruct_tool)

xg_reconstruct_tool.operations (del_non_keyframes = True, triangulate = True, reelin = True, deform = True)
