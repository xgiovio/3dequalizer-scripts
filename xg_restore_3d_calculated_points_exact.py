#
#
# 3DE4.script.name:	Restore 3D Points + Set Exact Survey
#
# 3DE4.script.version:	v1.0
#
# 3DE4.script.gui:	Orientation Controls::Edit
#
# 3DE4.script.comment:	Restore 3D Calculated Positions of all selected points from memory plus set exact survey to all of them
# 3DE4.script.gui.button:	Orientation Controls::Restore 3D Points + Set Exact Survey, align-bottom-left, 200, 20
#
#
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#

import xg_store_restore_3d_points
xg_store_restore_3d_points = reload(xg_store_restore_3d_points)

xg_store_restore_3d_points.restore_3d (exact = True) 







