#
#
# 3DE4.script.name:	Store 3D Points
#
# 3DE4.script.version:	v1.0
#
# 3DE4.script.gui:	Orientation Controls::Edit
#
# 3DE4.script.comment:	Store 3D Calculated Positions of all selected points in Memory
# 3DE4.script.gui.button:	Orientation Controls::Store 3D Points, align-bottom-left, 120, 20
#
#
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#

import xg_store_restore_3d_points
xg_store_restore_3d_points = reload(xg_store_restore_3d_points)

xg_store_restore_3d_points.store_3d ()






