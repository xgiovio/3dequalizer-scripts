# 3DE4.script.hide: true
# 3DE4.script.startup: false
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#

import tde4

global xg_3d_calculated_points_collection
try:
		xg_3d_calculated_points_collection
except :
		xg_3d_calculated_points_collection = {}

cam = tde4.getCurrentCamera()
current_point_group = tde4.getCurrentPGroup()
selected_points = tde4.getPointList (current_point_group,1)

def restore_3d (exact = False) :
	for point in selected_points:
		if point in xg_3d_calculated_points_collection:
			tde4.setPointSurveyPosition3D(current_point_group,point,xg_3d_calculated_points_collection[point])
			tde4.setPointCalcPosition3D(current_point_group,point,xg_3d_calculated_points_collection[point])
			calculated_status = tde4.getPointCalculated3DStatus(current_point_group,point)
			if calculated_status == "NOT_CALCULATED":
				tde4.setPointCalculated3D(current_point_group,point,1)
				tde4.setPointCalculated3DStatus(current_point_group,point,"CALCULATED")
			if exact:
				tde4.setPointSurveyMode(current_point_group,point,"SURVEY_EXACT")


def store_3d ():
	for point in selected_points:
		if tde4.isPointCalculated3D(current_point_group,point):
			survey_mode = tde4.getPointSurveyMode(current_point_group,point)
			if survey_mode != "SURVEY_EXACT":
				xg_3d_calculated_points_collection[point] = tde4.getPointCalcPosition3D(current_point_group,point)
			else:
				xg_3d_calculated_points_collection[point] = tde4.getPointSurveyPosition3D(current_point_group,point)







