#
#
# 3DE4.script.hide: true
# 3DE4.script.startup: false
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#

#init
current_value =  tde4.getParameterAdjustCurrentValue()
best_value =  tde4.getParameterAdjustCurrentValue()
get_custom_data = tde4.getParameterAdjustCustomParameters()
running = tde4.getParameterAdjustPhaseRunningFlag()

# script specific
camera_id = get_custom_data
lens_id = tde4.getCameraLens(camera_id)

if (running):
	#calculating
	tde4.applyParameterAdjustDataModification("@" + lens_id + ".focal_length_cm",current_value / 10)
else:
	#transfer_to_3de
	tde4.setLensFocalLength(lens_id,best_value / 10)


