#
#
# 3DE4.script.name:	TestAdjGui
#
# 3DE4.script.version:		v1.0
#
# 3DE4.script.comment:	
#
# 3DE4.script.gui:	Main Window::xgiovio
#
# 3DE4.script.hide: false
# 3DE4.script.startup: false
#
# Giovanni Di Grezia 2016
# http://www.xgiovio.com
#

#init
script_base = tde4.get3DEInstallPath() + "/sys_data/py_scripts/"

#script related
adj_script_name = "xg_ui_adj_tests.py"

#script
tde4.createParameterAdjustScript(script_base + adj_script_name,"xg_focal_" + tde4.getCameraName(tde4.getCurrentCamera()),tde4.getCurrentCamera() ,10,20)